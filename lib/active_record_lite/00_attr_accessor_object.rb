class AttrAccessorObject
  def self.my_attr_accessor(*names)
    ivar_names = names.map { |name| "@#{name}" }

    ivar_names.each_with_index do |ivar, indx|
      define_method "#{names[indx]}=" do |arg|
        instance_variable_set(ivar, arg)
      end

      define_method names[indx] do
        instance_variable_get(ivar)
      end
    end

  end
end
