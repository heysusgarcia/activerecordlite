require_relative 'db_connection'
require 'active_support/inflector'
#NB: the attr_accessor we wrote in phase 0 is NOT used in the rest
#    of this project. It was only a warm up.

class SQLObject
  def self.columns
    return @cols unless @cols.nil?
    p cols_arr = DBConnection.execute2("SELECT * FROM #{table_name}").first
    @cols = cols_arr.map(&:to_sym)

    @cols.each do |col|
      define_method "#{col}" do
        self.attributes[col]
      end

      define_method "#{col}=" do |value|
        self.attributes[col] = value
      end
    end
  end

  def self.table_name=(table_name)
    @table_name = table_name
  end

  def self.table_name
    @table_name = self.to_s.tableize if @table_name.nil?
    @table_name
  end

  def self.all
    all = DBConnection.execute(<<-SQL)
    SELECT #{self.table_name}.*
    FROM #{self.table_name}
    SQL
    self.parse_all(all)
  end

  def self.parse_all(results)
    results.map { |obj| self.new(obj) }
  end

  def self.find(id)
    objs = DBConnection.execute(<<-SQL, id)
    SELECT #{self.table_name}.*
    FROM #{self.table_name}
    WHERE id = ?
    SQL
    self.parse_all(objs).first
  end

  def attributes
    @attributes ||= {}
  end

  def insert
    col_names = self.class.columns.join(", ")
    question_marks = (["?"] * attribute_values.length).join(", ")

    DBConnection.execute(<<-SQL, *attribute_values)
    INSERT INTO
     "#{self.class.table_name}" (#{col_names})
    VALUES
      (#{question_marks})
    SQL
    self.id = DBConnection.last_insert_row_id
  end

  def initialize(params = {})
    cols = self.class.columns

    params.each do |attr, value|
      raise "unknown attribute '#{attr}'" unless cols.include?(attr.to_sym)
      self.send("#{attr}=", value)
    end
  end

  def save
    if self.id.nil?
      self.insert
    else
      self.update
    end
  end

  def update
  set_line = self.class.columns.map { |attr_name| "#{attr_name} = ?" }.join(", ")

  DBConnection.execute(<<-SQL, *attribute_values, self.id)
    UPDATE #{self.class.table_name}
    SET #{set_line}
    WHERE id = ?
    SQL
  end

  def attribute_values
    self.class.columns.map { |attr| self.send(attr) }
  end
end
